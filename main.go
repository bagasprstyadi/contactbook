
package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"encoding/json"
)

// The person Type (more like an object)
type Person struct {
	ID 			string   `json:"id,omitempty"`
    Name  		string   `json:"name,omitempty"`
    Phone   	string 	 `json:"phone,omitempty"`
}

var people []Person

// Display a single data
// func GetPerson(w http.ResponseWriter, r *http.Request) {
//     params := mux.Vars(r)
//     for _, item := range people {
//         if item.ID == params["id"] {
//             json.NewEncoder(w).Encode(item)
//             return
//         }
//     }
//     json.NewEncoder(w).Encode(&Person{})
// }

// // Delete an item
// func DeletePerson(w http.ResponseWriter, r *http.Request) {
//     params := mux.Vars(r)
//     for index, item := range people {
//         if item.ID == params["id"] {
//             people = append(people[:index], people[index+1:]...)
//             break
//         }
//         json.NewEncoder(w).Encode(people)
//     }
// }

func hello(w http.ResponseWriter, r *http.Request) {
	log.Printf("Serving request: %s", r.URL.Path)
	host, _ := os.Hostname()
	fmt.Fprintf(w, "Hello, world!\n")
	fmt.Fprintf(w, "Version: 1.0.0\n")
	fmt.Fprintf(w, "Hostname: %s\n", host)
}

func getPostUpdateDelete(w http.ResponseWriter, r *http.Request) {

	switch r.Method {
	case "GET":
		json.NewEncoder(w).Encode(people)
	case "POST":
	    var person Person
	    var decoder = json.NewDecoder(r.Body)
	    decoder.Decode(&person)

	    people = append(people, person)
	    json.NewEncoder(w).Encode(people)

	case "PUT":
		var person Person
		_ = json.NewDecoder(r.Body).Decode(&person)
	        
	    for index, item := range people {
	        if item.ID == person.ID {
	            people[index].Name = person.Name
	            people[index].Phone = person.Phone
	        }
	    }

	    json.NewEncoder(w).Encode(people)
	    
	case "DELETE":
	    
	    var person Person
		_ = json.NewDecoder(r.Body).Decode(&person)
	        
	    for index, item := range people {
	        if item.ID == person.ID {
	            people = append(people[:index], people[index+1:]...)
            	break
	        }
	    }

	    json.NewEncoder(w).Encode(people)
	}
}

func main() {
	// use PORT environment variable, or default to 8080
	port := "8080"
	if fromEnv := os.Getenv("PORT"); fromEnv != "" {
		port = fromEnv
	}

	// register hello function to handle all requests
	server := http.NewServeMux()
	server.HandleFunc("/", hello)
	server.HandleFunc("/contact", getPostUpdateDelete)

	people = append(people, Person{ID: "1", Name: "Doe", Phone: "081218611736"})
    people = append(people, Person{ID: "2", Name: "Joe", Phone: "081231231232"})

	// start the web server on port and accept requests
	log.Printf("Server listening on port %s", port)
	err := http.ListenAndServe(":"+port, server)
	log.Fatal(err)
}